import { Injectable } from '@nestjs/common';
import { Pet, Prisma } from '@prisma/client';
import { PrismaService } from '../prisma.service';

@Injectable()
export class PetDaoService {
  constructor(private prisma: PrismaService) {}

  async pet(petWhereUniqueInput: Prisma.PetWhereUniqueInput): Promise<Pet | null> {
    return this.prisma.pet.findUnique({ where: petWhereUniqueInput });
  }

  async pets(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.PetWhereUniqueInput;
    where?: Prisma.PetWhereInput;
    orderBy?: Prisma.PetOrderByInput;
  }): Promise<[number, Pet[]]> {
    const { cursor, where } = params;
    return await this.prisma.$transaction([
      this.prisma.pet.count({ where, cursor }),
      this.prisma.pet.findMany(params),
    ]);
  }

  async createPet(data: Prisma.PetCreateInput): Promise<Pet> {
    return this.prisma.pet.create({ data });
  }

  async updatePet(params: { where: Prisma.PetWhereUniqueInput; data: Prisma.PetUpdateInput }): Promise<Pet> {
    const { where, data } = params;
    return this.prisma.pet.update({ data, where });
  }

  async deletePet(where: Prisma.PetWhereUniqueInput): Promise<Pet> {
    return this.prisma.pet.update({ where, data: { deletedAt: new Date().toJSON() } });
  }
}
