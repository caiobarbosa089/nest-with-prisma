import { Test, TestingModule } from '@nestjs/testing';
import { PetDaoService } from './pet-dao.service';

describe('PetDaoService', () => {
  let service: PetDaoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PetDaoService],
    }).compile();

    service = module.get<PetDaoService>(PetDaoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
