import { INestApplication, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  private logger = new Logger('PrismaService');
  async onModuleInit() {
    try {
      await this.$connect();
      this.logger.log(`[POSTGRES] Conected`);
    } catch (error) {
      this.logger.log(`[POSTGRES ERROR] ${error}`);
    }
  }

  async enableShutdownHooks(app: INestApplication) {
    this.$on('beforeExit', () => {
      app
        .close()
        .then(() => this.logger.log(`[POSTGRES] Closed`))
        .catch((err) => this.logger.log(`[POSTGRES ERROR] ${err}`));
    });
  }
}
