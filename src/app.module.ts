import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_FILTER } from '@nestjs/core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PetDaoService } from './db/pet-dao/pet-dao.service';
import { PrismaService } from './db/prisma.service';
import { DefaultExceptionsFilter } from './utils/default-exception.filter';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
  ],
  controllers: [AppController],
  providers: [
    PrismaService,
    PetDaoService,
    AppService,
    { provide: APP_FILTER, useClass: DefaultExceptionsFilter },
  ],
})
export class AppModule {}
