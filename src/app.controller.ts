import { Body, Delete, Param, ParseIntPipe, Post, Put, Query } from '@nestjs/common';
import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { PetDTO } from './DTO/petDTO';
import { PetPaginatedDTO } from './DTO/petsPaginatedDTO';

@ApiTags('pet')
@Controller('pet')
@Controller()
export class AppController {
  constructor(private readonly petService: AppService) {}

  @Get(':id')
  async petById(@Param('id', ParseIntPipe) id: number): Promise<any> {
    return await this.petService.petByUnique({ id });
  }

  @Get()
  async pets(@Query() query: PetPaginatedDTO): Promise<any> {
    return await this.petService.pets(query);
  }

  @Post()
  async create(@Body() body: PetDTO): Promise<any> {
    return await this.petService.create(body);
  }

  @Put()
  async update(@Body() body: PetDTO): Promise<any> {
    return await this.petService.update(body);
  }

  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id: number): Promise<any> {
    return await this.petService.delete({ id });
  }
}
