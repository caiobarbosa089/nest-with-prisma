export interface IPet {
  id?: number;
  name: string;
  description: string;
  age: number;
  contact?: string;
  adress?: string;
  deletedAt?: string | Date;
  createdAt?: string | Date;
  updatedAt?: string | Date;
}
