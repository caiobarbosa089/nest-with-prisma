import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';

export abstract class PetDTO {
  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  id: number;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  description: string;

  @ApiProperty()
  @ApiPropertyOptional()
  @Type(() => Number)
  @IsNumber()
  @IsOptional()
  age: number;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  contact: string;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  adress: string;
}
