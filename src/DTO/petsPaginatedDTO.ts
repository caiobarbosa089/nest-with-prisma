import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';
import { PaginationDTO } from 'src/utils/paginationDTO';

export abstract class PetPaginatedDTO extends PaginationDTO {
  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  description: string;

  @ApiProperty()
  @ApiPropertyOptional()
  @Type(() => Number)
  @IsNumber()
  @IsOptional()
  age: number;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  contact: string;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  adress: string;

  @ApiProperty()
  @ApiPropertyOptional()
  @Type(() => Date)
  @IsOptional()
  deletedAt: string | Date;

  @ApiProperty()
  @ApiPropertyOptional()
  @Type(() => Date)
  @IsOptional()
  createdAt: string | Date;

  @ApiProperty()
  @ApiPropertyOptional()
  @Type(() => Date)
  @IsOptional()
  updatedAt: string | Date;
}
