import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsNumber, IsString } from 'class-validator';

export abstract class PaginationDTO {
  @Type(() => Number)
  @IsNumber()
  @IsOptional()
  @ApiProperty()
  @ApiPropertyOptional()
  skip: number;

  @Type(() => Number)
  @IsNumber()
  @IsOptional()
  @ApiProperty()
  @ApiPropertyOptional()
  take: number;

  @IsString()
  @IsOptional()
  @ApiProperty()
  @ApiPropertyOptional()
  orderBy: string;
}
