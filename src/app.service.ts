import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PetDaoService } from './db/pet-dao/pet-dao.service';
import { IPet } from './interface/pet.interface';

@Injectable()
export class AppService {
  private logger = new Logger('PetService');

  constructor(private petDaoService: PetDaoService) {}

  async petByUnique(unique: Prisma.PetWhereUniqueInput): Promise<any> {
    try {
      const data = await this.petDaoService.pet(unique);
      if (!data) {
        throw new HttpException(
          { error: `Id ${unique.id} was not found`, status: HttpStatus.BAD_REQUEST },
          HttpStatus.BAD_REQUEST,
        );
      }

      if (data.deletedAt) {
        throw new HttpException(
          { error: `Id ${unique.id} was deleted in ${data.deletedAt}`, status: HttpStatus.BAD_REQUEST },
          HttpStatus.BAD_REQUEST,
        );
      }

      return { success: true, data };
    } catch (error) {
      this.logger.log(`petByUnique() - ${error}`);
      throw new HttpException({ error, status: HttpStatus.BAD_REQUEST }, HttpStatus.BAD_REQUEST);
    }
  }

  async pets(query: any): Promise<any> {
    try {
      let { skip, take, orderBy, ...filters } = query;
      skip = skip ?? 0;
      take = take ?? 10;
      const params = {
        skip,
        take,
        where: { ...filters, AND: [{ deletedAt: null }] },
        orderBy: orderBy ? { [orderBy]: 'asc' } : undefined,
      };
      const [total, data] = await this.petDaoService.pets(params);
      return { success: true, data, pagination: { total, skip, take } };
    } catch (error) {
      this.logger.log(`pets() - ${error}`);
      throw new HttpException({ error, status: HttpStatus.BAD_REQUEST }, HttpStatus.BAD_REQUEST);
    }
  }

  async create(body: IPet): Promise<any> {
    try {
      const { age, name, description, contact, adress } = body;
      const data = await this.petDaoService.createPet({ age, name, description, contact, adress });
      return { success: true, data };
    } catch (error) {
      this.logger.log(`create() - ${error}`);
      throw new HttpException({ error, status: HttpStatus.BAD_REQUEST }, HttpStatus.BAD_REQUEST);
    }
  }

  async update(body: IPet): Promise<any> {
    try {
      const { id } = body;

      const exist = await this.petDaoService.pet({ id });
      if (!exist) {
        throw new HttpException(
          { error: `Id ${id} was not found`, status: HttpStatus.BAD_REQUEST },
          HttpStatus.BAD_REQUEST,
        );
      }

      if (exist.deletedAt) {
        throw new HttpException(
          { error: `Id ${id} was deleted in ${exist.deletedAt}`, status: HttpStatus.BAD_REQUEST },
          HttpStatus.BAD_REQUEST,
        );
      }

      const { age, name, description, contact, adress } = body;
      const data = { age, name, description, contact, adress };

      const response = await this.petDaoService.updatePet({ where: { id }, data });
      return { success: true, data: response };
    } catch (error) {
      this.logger.log(`update() - ${error}`);
      throw new HttpException({ error, status: HttpStatus.BAD_REQUEST }, HttpStatus.BAD_REQUEST);
    }
  }

  async delete(unique: Prisma.PetWhereUniqueInput): Promise<any> {
    try {
      const exist = await this.petDaoService.pet(unique);
      if (exist.deletedAt) {
        throw new HttpException(
          { error: `Id ${unique.id} was deleted in ${exist.deletedAt}`, status: HttpStatus.BAD_REQUEST },
          HttpStatus.BAD_REQUEST,
        );
      }
      await this.petDaoService.deletePet(unique);
      return { success: true };
    } catch (error) {
      this.logger.log(`delete() - ${error}`);
      throw new HttpException({ error, status: HttpStatus.BAD_REQUEST }, HttpStatus.BAD_REQUEST);
    }
  }
}
