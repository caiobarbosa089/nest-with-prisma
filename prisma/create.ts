import { PrismaClient } from '@prisma/client';
import { config } from 'dotenv';

const prisma = new PrismaClient();

async function main() {
  config();
  console.log('Creating...');

  await prisma.$connect();

  /*********************************************************************/
  /* Dangerous zone: */
  /* Delete all tables before create */
  // Only uncomment these block code below if you want to delete for all existed tables

  // await prisma.pet.deleteMany({});

  /*********************************************************************/

  /* Pet create */
  await prisma.pet.create({ data: {
    name: '',
    description: '',
    age: 2,
    contact: '',
    adress: '',
  }});

  await prisma.pet.create({ data: {
    name: '',
    description: '',
    age: 2,
    contact: '',
    adress: '',
  }});

  await prisma.pet.create({ data: {
    name: '',
    description: '',
    age: 2,
    contact: '',
    adress: '',
  }});

  await prisma.pet.create({ data: {
    name: '',
    description: '',
    age: 2,
    contact: '',
    adress: '',
  }});

  await prisma.pet.create({ data: {
    name: '',
    description: '',
    age: 2,
    contact: '',
    adress: '',
  }});

  await prisma.pet.create({ data: {
    name: '',
    description: '',
    age: 2,
    contact: '',
    adress: '',
  }});

  console.log('Created successfully ...........................');
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.$disconnect();
  });
