# NestJS with Prisma

-  [Description](#description)
-  [Requirements](#requirements)
-  [Migrations](#migrations)
-  [Environments](#environments)
  - [Developing](#developing)
    - [Local](#local-development)
    - [Docker](#docker-development)
-  [Environment variables](#environment-variables)

## Description

Simple CRUD API created with NestJS and Prisma ORM

## Requirements

- Must have [NodeJs](https://nodejs.org/pt-br/download/package-manager/)  and  [NestJs](https://docs.nestjs.com/first-steps#setup) for local run.
- Must have [Docker](https://docs.docker.com/get-docker/) and [Docker compose](https://docs.docker.com/compose/install/) for docker.

## Migrations

- `yarn migration` to start postgres migration.
- `yarn prisma:create` to create some examples in postgres.

## Environments

### Developing

##### Local development

 - `yarn install` to install dependencies.
 - `yarn start:dev` to start application

##### Docker development

 - `cp .env.dev .env` for enviroment variables.
 - `docker-compose -f dev.yml up -d` to start postgres in docker.
 - `yarn install` to install dependencies.
 - `yarn start:dev` to start application


## Environment variables

```bash
# API
API_PORT=3000

# SWAGGER
SWAGGER_TITLE="NestJS with Prisma"
SWAGGER_DESCRIPTION=""
SWAGGER_DOCS="doc"
SWAGGER_EXTERNAL_DOC=""

# POSTGRES
POSTGRES_USER=admin
POSTGRES_PASSWORD=admin
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_DB=my_db
DATABASE_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?schema=public"
```
